defmodule Sorting do

  def quicksort([h|t]) do
    quicksort_r(t,h,[],[])
  end

  def quicksort([]) do
    []
  end

  defp quicksort_r([h | t], p, l1, l2) do
    cond do
      h <= p ->
        quicksort_r(t,p, [h] ++ l1, l2)
      true ->
        quicksort_r(t,p, l1, l2 ++ [h])
    end
  end

  defp quicksort_r([],p,l1,l2) do
    quicksort(l1) ++ [p] ++ quicksort(l2)
  end

  def mergesort(l) do
    cond do
      length(l) > 1 ->
        mergesort_r(l, div(length(l), 2))
      length(l) == 1 ->
        l
      true ->
        []
    end
  end

  defp mergesort_r(l, n) do
    {l1, l2} = Enum.split(l, n)
    juntar(mergesort(l1), mergesort(l2),[])
  end

  defp juntar([h1|t1], [h2|t2], lf) do
    cond do
      h1 <= h2 ->
        juntar(t1,[h2|t2],lf ++ [h1])
      true ->
        juntar([h1|t1],t2,lf ++ [h2])
    end
  end

  defp juntar([], [h|t], lf) do
    juntar([],t,lf ++ [h])
  end

  defp juntar([h|t], [], lf) do
    juntar(t,[],lf ++ [h])
  end

  defp juntar([],[],[h|t]) do
    [h|t]
  end
end
