defmodule Db do

  def new() do
    {:ok, pid} = Task.start_link(fn -> loop([]) end)
    pid
  end

  defp loop(l) do
    receive do
      {:get, key, caller} ->
        if l[key] != nil do
          send caller, {:ok, l[key]}
        else
          send caller, {:error, :not_found}
        end
          loop(l)
      {:put, key, value, caller} ->
        l2 = l -- [{key, l[key]}]
        l3 = l2 ++ [{key, value}]
        send caller, {:ok, self()}
        loop(l3)
      {:delete, key, caller} ->
        send caller, {self()}
        loop(l --[{key, l[key]}])
      {:match, element, caller} ->
        l2 = for {k,v} <- l, v == element do
          String.to_integer(to_string(k))
        end
          send caller, {l2}
      {:destroy, caller} ->
        send caller, {:ok}
        Process.exit(self(), :ok)
      end
    end

  def write(db_ref, key, element) do
    send db_ref, {:put, String.to_atom(to_string(key)), element, self()}
    receive do
      {:ok, pid} ->
      pid
    end
  end

  def read(db_ref,key) do
    send db_ref, {:get, String.to_atom(to_string(key)), self()}
    receive do
      {:ok, element} ->
        {:ok, element}
      {:error, :not_found} ->
        {:error, :not_found}
    end
  end

  def delete(db_ref, key) do
    send db_ref, {:delete, String.to_atom(to_string(key)), self()}
    receive do
      {pid} ->
        pid
    end
  end

  def match(db_ref, element) do
    send db_ref, {:match, element, self()}
    receive do
      {l} ->
        l
    end
  end

  def destroy(db_ref) do
  #  send db_ref, {:destroy, self()}
  #  receive do
  #   {:ok} ->
  #   :ok
  #  end
  end
end
