defmodule Echo do

  def start() do
    Process.flag(:trap_exit, true)
    pid = spawn(fn -> loop() end)
    Process.register(pid, :echo)
    :ok
  end

  defp loop do
    receive do
      {:print, term} ->
        IO.puts term
        loop()
      {:end} ->
        :ok
      {:EXIT, from_pid, reason} ->
        IO.puts("Exit reason: #{reason}")
    end
  end

  def stop() do
    send :echo, {:end, self()}
    Process.unregister(:echo)
    :ok
  end

  def print(term) do
    send :echo, {:print, term}
    :ok
  end

end
