defmodule Manipulating do
  def filter(l,n) do
    for x <- l, x<=n, do: x
  end

  def reverse(l) do
    reversing_list(l,[])
  end

  defp reversing_list([h| t],l2) do
    reversing_list(t,[h|l2])
  end

  defp reversing_list([],l) do
    l
  end

  def concatenate(l) do
    reverse(concatenating_lists(l,[]))
  end

  defp concatenating_lists([[h1 | h2]| t], l) do
    concatenating_lists([h2|t],[h1 | l])
  end

  defp concatenating_lists([[]| t], l) do
    concatenating_lists( t, l)
  end

  defp concatenating_lists([],l) do
    l
  end

  def flatten(l) do
      reverse(flatting_lists(l, []))
  end

  defp flatting_lists([[h|t]|t2],l) do
      flatting_lists([h|[t|t2]],l)
  end

  defp flatting_lists([[]|t],l) do
      flatting_lists(t,l)
  end

  defp flatting_lists([h|t],l) do
    flatting_lists(t,[h|l])
  end

  defp flatting_lists([],l) do
      l
  end
end
