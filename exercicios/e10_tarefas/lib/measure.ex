#require Manipulating
#require Sorting

defmodule Measure do
  require Manipulating
  require Sorting

  def run(lista, elementos) do
    datosPreparados = Enum.map(lista,fn(data_creation) ->
       Task.async(fn -> data_creation(elementos) end) end)
    |> Enum.map(&Task.await(&1,:infinity))
    IO.puts "|-----------------------------------------------|"
    tiempoMaximoDatos = maxTimeValue(datosPreparados)
    tiempoSegundosCreacion = tiempoMaximoDatos / 1000000
    IO.puts "| Creacion de datos               : #{tiempoSegundosCreacion} sec "
    tareas = :lists.zip(lista,datosPreparados)
    for {func,datos} <- tareas do
      {m,f} = func
      {d,t} = datos
      a = :erlang.timestamp()
      if (to_string(f) == "flatten") do
        Task.await(Task.async(m,f,[flatLists(2,elementos)]),10000)
        #Task.await(Task.async(m,f,[[]]),10000)
      else
        Task.await(Task.async(m,f,[d]),10000)
      end
      b = :erlang.timestamp()
      IO.puts "| #{to_string(m)} #{to_string(f)}     : #{:timer.now_diff(b,a) / 1000000} sec "
    end
    IO.puts "|-----------------------------------------------|"
  end

  defp flatLists(n,elementos) do
    l = for x <- 0..n do
          for y <- 0..elementos do
            :rand.uniform(1000)
          end
        end
  end

  defp maxTimeValue([h| t]) do
      {k,v} = h
      maxTimeValue(t, v)
  end

  defp maxTimeValue([h | t], max) do
      {k,v} = h
      if (v > max) do
        maxTimeValue(t, v)
      else
        maxTimeValue(t,max)
      end
  end

  defp maxTimeValue([], max) do
    max
  end

  defp data_creation(n) do
    l = []
    a = :erlang.timestamp()
    l = for x <- 1..n, do:
      l ++ :rand.uniform(1000) # crea valores entre 0 y 1000 y los mete en la lista
    b = :erlang.timestamp()
    diff = :timer.now_diff(b,a)
    {l , diff}
  end

end
