defmodule Ring do
  def start(n,m,msg) do
    pid = spawn(fn -> create_new_process(n,m,msg) end)

    Process.register(pid, :start_ring)
    :ok
  end

  defp holder(child) do
    receive do
      {msg, m} ->
		      if (m > 0) do
			         #IO.puts "#{inspect self()} - printed #{msg} (#{m})"
			         IO.puts msg
			         send child, {msg, m-1}
			         holder(child)
          else
			         send :start_ring, {:destroy}
               holder(child)
          end
	    {:destroy} ->
        Process.unregister(:start_ring)
    end
  end

  defp create_new_process(n, m, msg) when n==0 do
    #IO.puts "#{inspect self()} - is online (last)"
    #IO.puts "sending ok to parent"
    :timer.sleep(200)
    send :start_ring, {msg, m}
    holder(:start_ring)
  end

  defp create_new_process(n, m, msg) do
    :timer.sleep(200)
    child = spawn(fn -> create_new_process(n-1,m,msg) end)
    #IO.puts "#{inspect self()} - is online"
    holder(child)
  end
end
