defmodule Mi6 do
  require GenServer
  require Create
  require Db
  require Manipulating
  require Agent

  def fundar() do
    {_,_} = GenServer.start_link(Mi6, Db.new(),name: :mi6)
    :ok
  end

  def init(stack) do
    {:ok, stack}
  end

  def recrutar(axente, destino) do
      GenServer.cast(:mi6,{:recrutar, {axente,destino}})
      :ok
  end

  def asignar_mision(axente, mision) do
      GenServer.cast(:mi6,{:asignar, {axente, mision}})
      :ok
  end

  def mision([h | t], :espiar) do
      Manipulating.filter([h | t], h)
  end

  def mision(l, :contrainformar) do
      Manipulating.reverse(l)
  end

  def handle_cast({:recrutar, {axente, destino}}, stack) do
      lectura = Db.read(stack, axente)
      case lectura do
        {:ok, _} ->
          {:noreply, stack}
        {:error, :not_found} ->
          length = String.length(destino)
          l = Enum.shuffle(Create.create(length))
          {:ok, pid} = Agent.start_link(fn () -> l end)
          {:noreply, Db.write(stack, axente, pid)}
      end
  end
  
  def handle_cast({:asignar, {axente, mision}}, stack) do
      agent = Db.read(stack, axente)
      case agent do
          {:error, :not_found} ->
              {:noreply, stack}
          {:ok, pid}->
              Agent.cast(pid, Mi6, :mision, [mision])
              {:noreply, stack}
      end
  end

  def consultar_estado(axente) do
      GenServer.call(:mi6,{:consultar, axente})
  end

  def handle_call({:consultar, axente}, _from, stack) do
      agent = Db.read(stack, axente)
      case agent do
          {:error, :not_found} ->
              {:reply, :you_are_here_we_are_not, stack}
          {:ok, pid}->
              result = Agent.get(pid, fn stack -> stack end)
              {:reply, result, stack}
      end
  end

  def disolver() do
      GenServer.stop(:mi6, :normal)
      :ok
  end

end
