require Integer
defmodule Effects do

    def print(n) do
        _print(n,[])
    end

    defp _print(n,l) do
      cond do
        n <= 0 ->
            IO.puts l
        n == 1 ->
            _print(n-1,[to_string(n)] ++ l)
        true ->
            _print(n-1,["\n"] ++ [to_string(n)] ++ l)
      end
    end

    def even_print(n) do
      recursive_even_print(n,[])
    end

    defp recursive_even_print(n,l) do
        cond do
            n < 2 ->
                IO.puts l
            n == 2 ->
                recursive_even_print(n-2,[to_string(n)] ++ l)
            Integer.is_even(n) ->
                recursive_even_print(n-2,["\n"] ++ [to_string(n)] ++ l)
            true ->
                recursive_even_print(n-1,l)
                
        end
    end
end
